# See: https://nixos.org/wiki/FAQ#How_can_I_manage_software_with_nix-env_like_with_configuration.nix.3F
#
# nix-env -f ~/packages.nix -i
with (import <nixpkgs> {});
{
  inherit
    bashmount
    cabal2nix
    chromium
    direnv
    # emacs-env
    fd
    firefox
    git
    git-crypt
    gnumake
    gnupg22
    google-chrome
    httpie
    keepassxc
    lsof # for git-annex
    mr
    nixops
    pandoc
    ripgrep
    signal-desktop
    slack
    # steam
    tmux
    tree
    unzip
    vcsh
    # python3.7-yubikey-manager
    espeak-ng
    ;

  git-annex = gitAndTools.git-annex;
  gnome-disk-utility = gnome3.gnome-disk-utility;
  # gnome-tweak-tool = gnome3.gnome-tweak-tool;
}
