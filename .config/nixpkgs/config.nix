{
  allowBroken = true;
  allowUnfree = true; # sadly, just for steam though

  packageOverrides = pkgs: with pkgs; rec {
    libbluray = pkgs.libbluray.override {
      # withJava = true;
      withAACS = true;
      withBDplus = true;
    };

    vlc = pkgs.vlc.override { inherit libbluray; };

    # from https://nixos.org/wiki/TexLive_HOWTO
    myTexLive = with pkgs; (texlive.combine {
      inherit (texlive) latexmk paralist mathpazo scheme-small moderncv;
    });

    myHaskellDevTools = haskellPkgs : with haskellPkgs; [
      # apply-refact # for hlint-refactor in emacs
      # ghc-mod
      # hasktags
      # hindent
      # hindent_5_2_1
      hlint
      stylish-haskell
      # structured-haskell-mode
      # brittany
      # (callCabal2nix "brittany"
      #   (pkgs.fetchFromGitHub {
      #     owner  = "lspitzner";
      #     repo   = "brittany";
      #     rev    = "6c187da8f8166d595f36d6aaf419370283b3d1e9";
      #     sha256 = "0nmnxprbwws3w1sh63p80qj09rkrgn9888g7iim5p8611qyhdgky";
      #   }) {})
    ];

    # https://github.com/jwiegley/nix-config/blob/master/config.nix#L167
    haskellFilterSource = paths: src: builtins.filterSource (path: type:
      let baseName = baseNameOf path; in
      !( type == "directory"
        && builtins.elem baseName ([".git" ".cabal-sandbox" "dist"] ++ paths))
      &&
      !( type == "unknown"
        || stdenv.lib.hasSuffix ".hdevtools.sock" path
        || stdenv.lib.hasSuffix ".sock" path
        || stdenv.lib.hasSuffix ".hi" path
        || stdenv.lib.hasSuffix ".hi-boot" path
        || stdenv.lib.hasSuffix ".o" path
        || stdenv.lib.hasSuffix ".o-boot" path
        || stdenv.lib.hasSuffix ".dyn_o" path
        || stdenv.lib.hasSuffix ".p_o" path))
      src;

    inherit (import /etc/nixos/common/emacs.nix { pkgs = pkgs; }) emacs emacs-env;
  };
}
